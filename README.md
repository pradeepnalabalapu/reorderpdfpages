## Script to reorder pages for Dane County Fair flyer print

### How to setup the environment

Make sure python is version 3.x

1. Create a virtual environment
``` 
python -m venv test_env
source test_env/bin/activate
pip install -r requirements.txt
```

2. Rename the pdfs into the following names and place in the top directory of this git repo
```
ColorSection.pdf
BWSection.pdf
Cover.pdf
```

3. Run the script
```
python orderedPages.py
```

4. The desired output should be in the file `output.pdf` in the same directory as above

