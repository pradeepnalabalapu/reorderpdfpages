#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 26 12:08:18 2019

@author: pradeep.nalabalapu
"""
import PyPDF2
import copy
import traceback

DEBUG = True

merge = False

COLORED_FILENAME="ColorSection.pdf"
BW_FILENAME="BWsection.pdf"
COVER_FILENAME="Cover.pdf"
OUTPUT_FILENAME="output.pdf"

WATERMARK_FILE1="sampleBlue.pdf"

## Preprocessing
##  split pages in original document into 4
##  example mutool poster -x 2 -y 2 BWsection_orig.pdf BWsection.pdf
##

def getOrderedPages(filename) :
    file = open(filename, 'rb')
    reader = PyPDF2.PdfFileReader(file)
    in_pages = split_each_page_into_4(reader.pages)
    n = len(in_pages)
    if DEBUG : print("Num pages in file {} = {}".format(filename,n))
    out_pages = []
    
    try:
        if n==4 :
            out_pages.append(in_pages[3])
            out_pages.append(in_pages[1].rotateClockwise(180))
            out_pages.append(in_pages[0].rotateClockwise(180))
            out_pages.append(in_pages[2])
            
            return out_pages, file
        for i in range(0,n,8):
            if i == 80 : #adjusting for bug in pdf bwSection
                out_pages.append(in_pages[i+4].rotateClockwise(180))
                out_pages.append(in_pages[i+1].rotateClockwise(180))
                out_pages.append(in_pages[i+3])
                out_pages.append(in_pages[i+6])
                out_pages.append(in_pages[i+7])
                out_pages.append(in_pages[i+2])
                out_pages.append(in_pages[i+0].rotateClockwise(180))
                out_pages.append(in_pages[i+5].rotateClockwise(180))
                continue
            out_pages.append(in_pages[i+0].rotateClockwise(180))
            out_pages.append(in_pages[i+5].rotateClockwise(180))
            out_pages.append(in_pages[i+7])
            out_pages.append(in_pages[i+2])
            out_pages.append(in_pages[i+3])
            out_pages.append(in_pages[i+6])
            out_pages.append(in_pages[i+4].rotateClockwise(180))
            out_pages.append(in_pages[i+1].rotateClockwise(180))
    except Exception as e:
        print(f"For filename={filename} received exception: {e}")
        traceback.print_exc()
        raise e

        
    return out_pages, file
    
    
def orderPdfPages() :
    
    bwPages, bwFile = getOrderedPages(BW_FILENAME)
    coloredPages, coloredFile = getOrderedPages(COLORED_FILENAME)
    coverPages, coverFile = getOrderedPages(COVER_FILENAME)
    
    # wm1Page = PyPDF2.PdfFileReader(open(WATERMARK_FILE1,"rb")).getPage(0)
    
    nBw = len(bwPages)
    nColored = len(coloredPages)
    nCover = len(coverPages)
    
    pdfWriter = PyPDF2.PdfFileWriter() 
    
    for page in coverPages[0:nCover//2] :
        if merge : page.mergePage(wm1Page)
        pdfWriter.addPage(page)
    
    for page in bwPages[0:nBw//2] :
        if merge : page.mergePage(wm1Page)
        pdfWriter.addPage(page)
    
    for page in coloredPages:
        if merge : page.mergePage(wm1Page)
        pdfWriter.addPage(page)
    
    for page in bwPages[nBw//2:]:
        if merge : page.mergePage(wm1Page)
        pdfWriter.addPage(page)
    
    for page in coverPages[nCover//2:]:
        if merge : page.mergePage(wm1Page)
        pdfWriter.addPage(page)
    
    outputFile = open(OUTPUT_FILENAME, "wb")
    pdfWriter.write(outputFile)
    
    bwFile.close()
    coloredFile.close()
    coverFile.close()
    outputFile.close()


def split_each_page_into_4(pages):
    split_pages = []
    for page in pages:
        x0, y0 = page.mediaBox.lowerLeft
        x1, y1 = page.mediaBox.upperRight

        for k in reversed(range(2)): # top , bottom
            for j in range(2): # left, right
                page_cp = copy.copy(page)
                page_cp.mediaBox = copy.copy(page.mediaBox)
                page_cp.mediaBox.upperRight = (x0 + (x1-x0)*(j+1)//2,
                                               y0 + (y1-y0)*(k+1)//2)
                page_cp.mediaBox.lowerLeft = (x0 + (x1-x0)*j//2,
                                              y0 + (y1-y0)*k//2)
                split_pages.append(page_cp)
    return split_pages


if __name__ == "__main__":
    orderPdfPages()
